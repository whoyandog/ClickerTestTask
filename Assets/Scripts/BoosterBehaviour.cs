using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterBehaviour : MonoBehaviour
{
    private GameObject booster;
    public GameBehaviour game;

    public void Initialize(GameObject booster, GameBehaviour game)
    {
        this.booster = booster;
        this.game = game;
    }

    void OnMouseDown()
    {
        game.BoosterSetActive(booster);
    }
}
