using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuNavigation : MonoBehaviour
{
    public GameObject main;
    public GameObject scores;
    public GameObject credits;

    public TextMeshProUGUI labelScores;

    void Awake() {
        main.SetActive(true);
        scores.SetActive(false);
        credits.SetActive(false);
    }

    public void LoadSceneByID(int i) {
        SceneManager.LoadScene(i);
    }

    public void ScoresOpen() {
        main.SetActive(false);
        scores.SetActive(true);

        labelScores.text = "Best score\n\n" + SaveManager.LoadScore();
    }

    public void ScoresBack() {
        scores.SetActive(false);
        main.SetActive(true);
    }

    public void CreditsOpen() {
        main.SetActive(false);
        credits.SetActive(true);
    }

    public void CreditBack() {
        main.SetActive(true);
        credits.SetActive(false);
    }

    public void QuitGame () {
        Application.Quit();
    }
}
