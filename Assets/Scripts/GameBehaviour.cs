using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameBehaviour : MonoBehaviour
{
    public GameObject enemyPrefabEasy;
    public GameObject enemyPrefabMiddle;
    public GameObject enemyPrefabHard;
    public GameObject boosterKillAll;
    public GameObject boosterFreeze;
    public TextMeshProUGUI labelScores;
    public TextMeshProUGUI labelEnemies;
    public TextMeshProUGUI labelMessages;
    public GameObject gameOverPanel;
    public GameObject gamePausePanel;
    public GameObject pauseButton;

    List<GameObject> pool = new List<GameObject>();
    List<GameObject> poolAlive = new List<GameObject>();
    GameObject poolObj;
    GameObject aliveObj;
    float timer = .0f;
    float timerMessages = .0f;

    private int scores = 0;
    private int enemiesOnScreen = 0;

    private bool isGameOver = false;

    void Awake()
    {
        poolObj = new GameObject("POOL");
        aliveObj = new GameObject("ALIVE");
        InitializePool();
        InitializeBoosters();
        Time.timeScale = 1;
    }

    void Update()
    {
        if (!isGameOver)
        {
            RandomSpawn();
            DeleteMessage();

            if (enemiesOnScreen == 10)
            {
                GameOver();
            }
        }
    }

    void InitializePool()
    {
        AddEnemies(6, 1, 30, enemyPrefabEasy, PrimitiveType.Cube);
        AddEnemies(3, 2, 15, enemyPrefabMiddle, PrimitiveType.Capsule);
        AddEnemies(1, 3, 30, enemyPrefabMiddle, PrimitiveType.Sphere);
    }

    void AddEnemies(int count, int health, int speed, GameObject prefab, PrimitiveType primitive)
    {
        GameObject enemy;

        for (int i = 0; i < count; i++)
        {
            if (prefab == null)
            {
                enemy = GameObject.CreatePrimitive(primitive);
            }
            else
            {
                enemy = Instantiate(prefab);
            }
            enemy.transform.parent = poolObj.transform;
            enemy.AddComponent<Rigidbody>();
            var enemyBehaviour = enemy.AddComponent<EnemyBehaviour>();
            enemyBehaviour.game = this;
            enemyBehaviour.enemy = enemy;
            pool.Add(enemy);

            enemyBehaviour.Initilize(health, speed);

            enemy.SetActive(false);
        }
    }

    void InitializeBoosters()
    {
        boosterFreeze = Instantiate(boosterFreeze);
        boosterKillAll = Instantiate(boosterKillAll);

        boosterFreeze.SetActive(false);
        boosterKillAll.SetActive(false);

        boosterFreeze.AddComponent<BoosterBehaviour>().Initialize(boosterFreeze, this);
        boosterKillAll.AddComponent<BoosterBehaviour>().Initialize(boosterKillAll, this);
    }

    void boosterCheck()
    {
        if (scores > 0 && scores % 10 == 0)
        {
            if (!boosterFreeze.activeSelf) // metka
            {
                boosterFreeze.SetActive(true);
                boosterFreeze.transform.position = new Vector3(
                    Random.Range(-4, 4),
                    0,
                    Random.Range(-8, 8)
                );
            }
        }
        if (scores > 0 && scores % 15 == 0)
        {
            if (!boosterKillAll.activeSelf)
            {
                boosterKillAll.SetActive(true);
                boosterKillAll.transform.position = new Vector3(
                    Random.Range(-4, 4),
                    0,
                    Random.Range(-8, 8)
                );
            }
        }
    }

    GameObject TakeFromPool()
    {
        int index = Random.Range(0, pool.Count);
        var enemy = pool[index];
        poolAlive.Add(pool[index]);
        pool.RemoveAt(index);

        enemy.SetActive(true);
        enemy.transform.parent = aliveObj.transform;
        enemy.GetComponent<EnemyBehaviour>().Activate();

        return enemy;
    }

    void RandomSpawn()
    {
        if (pool.Count != 0)
        {
            if (timer <= 0f)
            {
                TakeFromPool().transform.position = new Vector3(
                    Random.Range(-4f, 4f),
                    0,
                    Random.Range(-8f, 8f)
                );
                timer = Random.Range(0.5f, 3f);
            }
            timer -= Time.deltaTime;
        }
    }

    void GameOver()
    {
        isGameOver = true;
        gameOverPanel.SetActive(true);
        gameOverPanel.GetComponentInChildren<TextMeshProUGUI>().text += scores;

        if (SaveManager.LoadScore() < scores)
            SaveManager.SaveScore(scores);
    }

    void DeleteMessage()
    {
        timerMessages -= Time.deltaTime;
        if (timerMessages <= 0)
        {
            labelMessages.text = "";
        }
    }

    public void PauseGame()
    {
        gamePausePanel.SetActive(true);
        pauseButton.SetActive(false);

        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        gamePausePanel.SetActive(false);
        pauseButton.SetActive(true);

        Time.timeScale = 1;
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void BoosterSetActive(GameObject booster)
    {
        booster.SetActive(false);
        if (booster.tag == "Freeze")
        {
            timer += 3;

            labelMessages.text = "Freeze 3s";

            timerMessages = 3;
        }
        if (booster.tag == "KillAll")
        {
            foreach (var p in poolAlive)
            {
                p.GetComponent<EnemyBehaviour>().Kill();

                labelMessages.text = $"Kill all enemies";

                timerMessages = 3;
            }
        }
    }

    public void BackToPool(GameObject enemy)
    {
        enemy.SetActive(false);
        enemy.transform.position = new Vector3(0, 100, 0);

        enemy.transform.parent = poolObj.transform;
        poolAlive.Remove(enemy);
        pool.Add(enemy);

        scores++;
        labelScores.text = "Scores: " + scores;

        boosterCheck();
    }

    public int GetScores()
    {
        return scores;
    }

    public void EnemiesOnScreen(int e = 0)
    {
        enemiesOnScreen += e;
        labelEnemies.text = "Enemies: " + enemiesOnScreen;
    }

    public bool GetGameOverStatus()
    {
        return isGameOver;
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

public static class SaveManager
{
    public static void SaveScore(int score)
    {
        PlayerPrefs.SetInt("score", score);
        PlayerPrefs.Save();
    }

    public static int LoadScore()
    {
        if (!PlayerPrefs.HasKey("score"))
            return 0;
        return PlayerPrefs.GetInt("score");
    }
}