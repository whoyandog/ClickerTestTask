using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    private int health, baseHealth;
    private float speed, baseSpeed;
    private Rigidbody body;
    private bool isOnScreeen;

    public GameBehaviour game;

    public GameObject enemy;

    public bool isActive = false;

    public void Initilize(int baseHealth = 1, float baseSpeed = 1)
    {
        body = this.GetComponent<Rigidbody>();
        body.useGravity = false;
        body.mass = 50;

        this.baseHealth = baseHealth;
        this.baseSpeed = baseSpeed;

        transform.position = new Vector3(0, 100, 0);
    }

    void Update()
    {
        CheckHealth();
        IsEnemyOnScreen();
    }

    void FixedUpdate()
    {
        MoveToCenter();
    }

    void Died()
    {
        isActive = false;
        game.BackToPool(enemy);
    }

    public void CheckHealth()
    {
        if (isActive)
        {
            if (health <= 0)
            {
                Died();
            }
        }
    }

    public void Activate()
    {
        isActive = true;
        health = (int)Mathf.Round(baseHealth + (game.GetScores() / 10));
        speed = (float)game.GetScores() + baseSpeed;

        body.velocity = Vector3.zero;
    }

    public void Kill()
    {
        health -= 1000000;
    }

    void MoveToCenter()
    {
        body.AddForce((new Vector3(game.transform.position.x, 0, game.transform.position.z) - transform.position).normalized * speed, ForceMode.Force);
    }

    void OnMouseDown()
    {
        if (!game.GetGameOverStatus())
        {
            health--;
        }
    }

    void IsEnemyOnScreen()
    {
        var cameraPosition = game.GetComponent<Camera>().transform.position;

        float xOffset = 4f, zOffset = 8f;

        if (transform.position.x < cameraPosition.x + xOffset && transform.position.x > cameraPosition.x - xOffset)
        {
            if (transform.position.z < cameraPosition.z + zOffset && transform.position.z > cameraPosition.z - zOffset)
            {
                if (transform.position.y < cameraPosition.y && transform.position.y > -1f)
                {
                    if (!isOnScreeen)
                    {
                        isOnScreeen = true;
                        game.EnemiesOnScreen(1);
                    }
                }
                else
                {
                    if (isOnScreeen)
                    {
                        isOnScreeen = false;
                        game.EnemiesOnScreen(-1);
                    }
                }
            }
            else
            {
                if (isOnScreeen)
                {
                    isOnScreeen = false;
                    game.EnemiesOnScreen(-1);
                }
            }
        }
        else
        {
            if (isOnScreeen)
            {
                isOnScreeen = false;
                game.EnemiesOnScreen(-1);
            }
        }
    }
}
